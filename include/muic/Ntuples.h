//
// Created by omiguelc on 09/01/22.
//

#ifndef MUICPYTHIASIMULATIONS_NTUPLES_H
#define MUICPYTHIASIMULATIONS_NTUPLES_H

#include <vector>

#include "TTree.h"

using namespace std;

class OutputNtuple {
  public:
    OutputNtuple();

    ~OutputNtuple();

    double evt_weight_;

    double Q2_, W2_, x_, y_;
    double rec_Q2_, rec_x_, rec_y_, rec_agljb_;
    double mu_e_, mu_px_, mu_py_, mu_pz_, mu_phi_, mu_eta_;
    double s;

    int vsize_had_;
    vector<int> had_id_;
    vector<double> had_e_, had_px_, had_py_, had_pz_, had_phi_, had_eta_,
        had_mass_;

    void registerBranches(TTree *tree);

    void clear();
};

#endif // MUICPYTHIASIMULATIONS_NTUPLES_H

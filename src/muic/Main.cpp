#include <any>
#include <map>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include "ROOT/TProcessExecutor.hxx"

#include "muic/Constants.h"
#include "muic/Simulation.h"

typedef struct {
    int run_number;
    int n_events;
    std::string input_lhef;
    std::string output_dir;
    std::string output_ext;
    std::string base_output_name;
    std::map<std::string, std::any> machine;
    std::map<std::string, std::any> fc_set;
} LHERunArgs;

typedef struct {
    mutable std::atomic_int total;
    mutable std::atomic_int ongoing;
    mutable std::atomic_int done;
} RunStats;

// FORWARD DECLARATIONS BECAUSE CPP IS DUMB
void runDISFromLHE();

// MAIN
int main() {
    runDISFromLHE();

    // RETURN
    return 0;
}

// METHODS

void runDISFromLHE() {
    // Run Options
    // User should specify the datasets they want to generate here.
    int n_events = 600000;
    std::string input_dir{"/home/prb5/"};
    std::string output_dir = "../out/";
    std::string output_ext = "_mg5_600k";

    std::vector<std::string> run_processes{{"LQ", "Int", "SM"}};
    std::vector<std::tuple<std::string, std::string>> run_fc_sets {{"nocuts", ""}};
    std::vector<std::tuple<std::string, std::string>> run_simulations{{"muic", "MuIC"}};

    // Build run args
    int job_number = 0;

    std::vector<LHERunArgs> run_args;

    for (auto &simulation : run_simulations) {
        std::string simulation_machine_name = std::get<0>(simulation);
        std::string simulation_dirname = std::get<1>(simulation);

        for (auto &fc_set : run_fc_sets) {
            std::string fc_set_name = std::get<0>(fc_set);
            std::string fc_set_dirext = std::get<1>(fc_set);

            // NC
            for (auto &process_dirname : run_processes) {
                std::string input_lhef{input_dir + process_dirname +
                                       "/Events/MuIC/unweighted_events.lhe.gz"};

                std::string output_base_name{simulation_dirname + "_" +
                                             process_dirname};

                std::cout << "Queued input: " << input_lhef << std::endl;

                run_args.push_back({job_number++, n_events, input_lhef,
                                    output_dir, output_ext, output_base_name,
                                    machines[simulation_machine_name],
                                    fiducial_cut_sets[fc_set_name]});
            }
        }
    }

    std::cout << "Generating " << run_args.size() << " datasets." << std::endl;

    // DEFINE JOB FUNCTION
    auto run_stats = RunStats();
    run_stats.total = run_args.size();
    run_stats.ongoing = 0;
    run_stats.done = 0;

    auto run_function = [&run_stats](LHERunArgs args) {
        // Get & Update Counters
        run_stats.ongoing++;

        // Get parameters
        auto &run_number = args.run_number;
        auto &n_events = args.n_events;
        auto &output_dir = args.output_dir;
        auto &output_ext = args.output_ext;
        auto &machine = args.machine;
        auto &fc_set = args.fc_set;
        auto &input_lhef = args.input_lhef;
        auto &base_output_name = args.base_output_name;

        // Build output name
        std::string output_fiducial_cut_set_name =
            std::any_cast<const char *>(fc_set["output"]);
        std::string output_name = base_output_name + "_" +
                                  output_fiducial_cut_set_name + output_ext +
                                  ".root";

        // Log
        std::cout << "Run " << run_number << " begin." << std::endl;
        std::cout << "Runs Remaining To Process: "
                  << (run_stats.total - run_stats.done - run_stats.ongoing)
                  << std::endl;
        std::cout << "Run " << run_number << " Output: " << output_name
                  << std::endl;

        // Configure
        Simulation simulation;
        simulation.setOutputPath(output_dir + output_name);
        simulation.setNEvents(n_events);
        simulation.setLHEInputFile(input_lhef);
        simulation.setUseColorReconnectionFixEn(false);
        simulation.setEProton(std::any_cast<double>(machine["eProton"]));
        simulation.setEMuon(std::any_cast<double>(machine["eMuon"]));
        simulation.setQ2Min(std::any_cast<double>(fc_set["Q2Min"]));
        simulation.setYMin(std::any_cast<double>(fc_set["yMin"]));
        simulation.setYMax(std::any_cast<double>(fc_set["yMax"]));

        // Run
        simulation.run();

        // Update Counters
        run_stats.ongoing--;
        run_stats.done++;

        // Log
        std::cout << "Run " << run_number << " end." << std::endl;
        std::cout << "Runs Remaining to Complete: "
                  << (run_stats.total - run_stats.done) << std::endl;

        return 0;
    };

    // GO BRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    auto hardware_concurrency = std::thread::hardware_concurrency();

    std::cout << "Cores " << hardware_concurrency << " that wil go brrrr."
              << std::endl;

    ROOT::TProcessExecutor workers(hardware_concurrency);

    workers.Map(run_function, run_args);
}
//
// Created by omiguelc on 09/01/22.
//

#include "muic/Ntuples.h"

OutputNtuple::OutputNtuple() { clear(); }

OutputNtuple::~OutputNtuple() = default;

void OutputNtuple::registerBranches(TTree *tree) {
    tree->Branch("evt_weight", &evt_weight_);

    tree->Branch("Q2", &Q2_);
    tree->Branch("W2", &W2_);
    tree->Branch("x", &x_);
    tree->Branch("y", &y_);

    tree->Branch("rec_Q2", &rec_Q2_);
    tree->Branch("rec_x", &rec_x_);
    tree->Branch("rec_y", &rec_y_);
    tree->Branch("rec_agljb", &rec_agljb_);

    tree->Branch("mu_e", &mu_e_);
    tree->Branch("mu_px", &mu_px_);
    tree->Branch("mu_py", &mu_py_);
    tree->Branch("mu_pz", &mu_pz_);
    tree->Branch("mu_phi", &mu_phi_);
    tree->Branch("mu_eta", &mu_eta_);

    tree->Branch("vsize_had", &vsize_had_);
    tree->Branch("had_id", &had_id_);
    tree->Branch("had_e", &had_e_);
    tree->Branch("had_px", &had_px_);
    tree->Branch("had_py", &had_py_);
    tree->Branch("had_pz", &had_pz_);
    tree->Branch("had_phi", &had_phi_);
    tree->Branch("had_eta", &had_eta_);
    tree->Branch("had_mass", &had_mass_);

    tree->Branch("s", &s);
}

void OutputNtuple::clear() {
    evt_weight_ = 0.;

    Q2_ = -1;
    W2_ = -1;
    x_ = -1;
    y_ = -1;

    rec_Q2_ = -1;
    rec_x_ = -1;
    rec_y_ = -1;
    rec_agljb_ = -1;

    mu_e_ = -1;
    mu_px_ = -1;
    mu_py_ = -1;
    mu_pz_ = -1;
    mu_phi_ = -1;
    mu_eta_ = -1;

    vsize_had_ = 0;
    had_id_.clear();
    had_e_.clear();
    had_px_.clear();
    had_py_.clear();
    had_pz_.clear();
    had_phi_.clear();
    had_eta_.clear();
    had_mass_.clear();
}

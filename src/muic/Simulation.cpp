//
// Created by omiguelc on 08/02/22.
//

#include "muic/Simulation.h"

#include <any>

#include "Pythia8/Pythia.h"
#include "boost/algorithm/string_regex.hpp"

#include "TF1.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom3.h"
#include "TTree.h"

#include "muic/Ntuples.h"

using namespace Pythia8;

Simulation::Simulation() {
    output_path_ = "default.root";
    n_events_ = 50000;
    lhe_input_fpath_ = "";
    e_proton_ = 275; // GeV
    e_muon_ = 960;   // GeV
    Q2_min_ = 1;     // GeV^2
    y_min_ = 0.01;
    y_max_ = 0.99;
    use_color_reconnection_fix_en_ = false;
    use_charged_current_en_ = true;
    use_neutral_current_en_ = false;
    use_antimuon_en_ = false;
}

const string &Simulation::getOutputPath() const { return output_path_; }

void Simulation::setOutputPath(const string &outputPath) {
    output_path_ = outputPath;
}

int Simulation::getNEvents() const { return n_events_; }

void Simulation::setNEvents(int nEvents) { n_events_ = nEvents; }

string Simulation::getLHEInputFile() const { return lhe_input_fpath_; }

void Simulation::setLHEInputFile(string file_path) {
    lhe_input_fpath_ = file_path;
}

double Simulation::getEProton() const { return e_proton_; }

void Simulation::setEProton(double eProton) { e_proton_ = eProton; }

double Simulation::getEMuon() const { return e_muon_; }

void Simulation::setEMuon(double eMuon) { e_muon_ = eMuon; }

double Simulation::getQ2Min() const { return Q2_min_; }

void Simulation::setQ2Min(double q2Min) { Q2_min_ = q2Min; }

double Simulation::getYMin() const { return y_min_; }

void Simulation::setYMin(double yMin) { y_min_ = yMin; }

double Simulation::getYMax() const { return y_max_; }

void Simulation::setYMax(double yMax) { y_max_ = yMax; }

bool Simulation::isUseAntimuonEn() const { return use_antimuon_en_; }

void Simulation::setUseColorReconnectionFixEn(bool enable) {
    use_color_reconnection_fix_en_ = enable;
}

bool Simulation::isUseColorReconnectionFixEn() const {
    return use_color_reconnection_fix_en_;
}

void Simulation::setUseAntimuonEn(bool enable) { use_antimuon_en_ = enable; }

bool Simulation::isUseChargedCurrentEn() const {
    return use_charged_current_en_;
}

void Simulation::setUseChargedCurrentEn(bool enable) {
    use_charged_current_en_ = enable;
}

bool Simulation::isUseNeutralCurrentEn() const {
    return use_neutral_current_en_;
}

void Simulation::setUseNeutralCurrentEn(bool enable) {
    use_neutral_current_en_ = enable;
}

void Simulation::run() {
    // Muon ID
    int muon_id = 13;

    if (use_antimuon_en_) {
        muon_id = -muon_id;
    }

    // Calculate s
    double m_proton = 0.938272; // GeV/c^2
    double p_proton = sqrt(pow(e_proton_, 2) - pow(m_proton, 2));

    double m_muon = 0.105658; // GeV/c^2
    double p_muon = sqrt(pow(e_muon_, 2) - pow(m_muon, 2));

    double s = pow(e_muon_ + e_proton_, 2) - pow(p_muon - p_proton, 2);

    // Generator.
    Pythia pythia;

    if (lhe_input_fpath_ == "") {
        // Set up incoming beams, for frame with unequal beam energies.
        pythia.readString("Beams:frameType = 2");

        // BeamA = proton.
        pythia.readString("Beams:idA = 2212");
        pythia.settings.parm("Beams:eA", e_proton_);

        // BeamB = muon.
        pythia.readString("Beams:idB = " + std::to_string(muon_id));
        pythia.settings.parm("Beams:eB", e_muon_);

        // Set up DIS process within some phase space.
        // Neutral current: Scattering f f' → f f' via gamma^*/Z^0 t-channel
        // exchange, with full interference between the gamma^* and Z^0
        if (use_neutral_current_en_) {
            pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");
        }

        // Charged current: Scattering f_1 f_2 → f_3 f_4 via W^+- t-channel
        // exchange
        if (use_charged_current_en_) {
            pythia.readString("WeakBosonExchange:ff2ff(t:W) = on");
        }

        // Set renormalization scale
        pythia.readString("SigmaProcess:renormScale2 = 6");
        pythia.readString("SigmaProcess:factorScale2 = 6");

        // Phase-space cut: minimal Q2 of process.
        pythia.settings.parm("PhaseSpace:Q2Min", Q2_min_);
    } else {
        pythia.readString("Beams:frameType = 4");
        pythia.readString("Beams:LHEF = " + lhe_input_fpath_);

        // Specify one must read inputs from the MadGraph banner.
        pythia.readString("JetMatching:setMad=off");

        // Disable match in and out
        pythia.readString("LesHouches:matchInOut = off");
    }

    // Disable annoying verbosity.
    pythia.readString("Next:numberCount = 0");

    // Set dipole recoil on. Necessary for DIS + shower.
    pythia.readString("SpaceShower:dipoleRecoil = on");

    // Allow emissions up to the kinematical limit,
    // since rate known to match well to matrix elements everywhere.
    pythia.readString("SpaceShower:pTmaxMatch = 2");

    // QED radiation off lepton not handled yet by the new procedure.
    pythia.readString("PDF:lepton = off");
    pythia.readString("TimeShower:QEDshowerByL = off");

    // For consistency with MG5 Studies
    // Can be commented out
    pythia.readString("PDF:pSet=LHAPDF6:PDF4LHC15_nlo_mc_pdfas");

    // These options are sometimes required when reading from LHE File.
    // Seems to not be needed when simulating directly from pythia,
    // but sometimes needed when reading from LHE File
    // Can be commented out
    if (use_color_reconnection_fix_en_) {
        pythia.readString("BeamRemnants:remnantMode=1");
        pythia.readString("ColourReconnection:mode=1");
    }

    // Initialize
    pythia.init();

    // Set up the ROOT TFile and TTree Output.
    auto *file = TFile::Open(output_path_.c_str(), "recreate");

    auto *meta_tree = new TTree("meta", "Metadata");

    int events_total = 0, events_processed = 0;
    double events_total_weighted = 0, events_processed_weighted = 0;

    meta_tree->Branch("events_total", &events_total);
    meta_tree->Branch("events_processed", &events_processed);
    meta_tree->Branch("events_total_weighted", &events_total_weighted);
    meta_tree->Branch("events_processed_weighted", &events_processed_weighted);

    auto *event_tree = new TTree("evt", "Event Tree");
    auto *output_ntuple = new OutputNtuple();
    output_ntuple->registerBranches(event_tree);

    // Begin event loop.
    auto &event = pythia.event;

    while (pythia.next() && events_processed < n_events_) {
        // CLEAR NTUPLE FOR NEXT EVENT
        output_ntuple->clear();

        // GET EVENT INFORMATION
        double event_weight = pythia.info.weight();

        // INCREASE EVENT TOTAL
        events_total++;
        events_total_weighted += event_weight;

        // Four-momenta of proton, muon, virtual photon/Z^0/W^+-.
        auto &proton_in = event[1];
        auto &muon_in = event[4];
        auto &muon_out = event[6];

        auto p_proton_in = proton_in.p();
        auto p_muon_in = muon_in.p();
        auto p_muon_out = muon_out.p();
        auto p_virtual = p_muon_in - p_muon_out;

        // Calculate Q2, W2, Bjorken x, y.
        double Q2 = -p_virtual.m2Calc();
        double W2 = (p_proton_in + p_virtual).m2Calc();
        double x = Q2 / (2. * p_proton_in * p_virtual);
        double y = (p_proton_in * p_virtual) / (p_proton_in * p_muon_in);

        // Calculate Q2, W2, Bjorken x, y Lepton Method.
        double rec_Q2 = 4. * p_muon_in.e() * p_muon_out.e() *
                        pow(cos(p_muon_out.theta() / 2.), 2);
        double rec_y = 1 - p_muon_out.e() * (1 - cos(p_muon_out.theta())) /
                               (2. * p_muon_in.e());
        double rec_x = rec_Q2 / (s * rec_y);

        double rec_agljb =
            2 * atan(1 / sqrt(4 * pow(p_proton_in.e(), 2) * pow(x, 2) / Q2 -
                              x * p_proton_in.e() / p_muon_in.e()));

        // SHORT-CIRCUIT
        if (rec_y < y_min_ || y_max_ < rec_y) {
            continue;
        }

        // INCREASE EVENTS PROCESSED
        events_processed++;
        events_processed_weighted += event_weight;

        if ((events_processed % 1000) == 0) {
            std::cout << "Events Processed: " << to_string(events_processed)
                      << std::endl;
        }

        // STORE EVENT INFORMATION
        output_ntuple->evt_weight_ = event_weight;

        // Fill kinematics histograms.
        output_ntuple->Q2_ = Q2;
        output_ntuple->W2_ = W2;
        output_ntuple->x_ = x;
        output_ntuple->y_ = y;

        output_ntuple->rec_Q2_ = rec_Q2;
        output_ntuple->rec_x_ = rec_x;
        output_ntuple->rec_y_ = rec_y;
        output_ntuple->rec_agljb_ = rec_agljb;

        output_ntuple->mu_e_ = muon_out.e();
        output_ntuple->mu_px_ = muon_out.px();
        output_ntuple->mu_py_ = muon_out.py();
        output_ntuple->mu_pz_ = muon_out.pz();
        output_ntuple->mu_phi_ = muon_out.phi();
        output_ntuple->mu_eta_ = muon_out.eta();

        output_ntuple->s = s;

        // Loop particles
        int particle_vsize = 0;

        for (int i = 0; i < event.size(); ++i) {
            // Short-Circuit: Skip all particles which aren't final,
            // and the original muon
            if (!event[i].isFinal() || event[i].id() == muon_id) {
                continue;
            }

            // Dump
            int particle_id = event[i].id();
            double particle_px = event[i].px();
            double particle_py = event[i].py();
            double particle_pz = event[i].pz();
            double particle_e = event[i].e();
            double particle_phi = event[i].phi();
            double particle_eta = event[i].eta();
            double particle_mass = event[i].m();

            // Store kinematics of final state particles
            output_ntuple->had_id_.push_back(particle_id);
            output_ntuple->had_e_.push_back(particle_e);
            output_ntuple->had_px_.push_back(particle_px);
            output_ntuple->had_py_.push_back(particle_py);
            output_ntuple->had_pz_.push_back(particle_pz);
            output_ntuple->had_phi_.push_back(particle_phi);
            output_ntuple->had_eta_.push_back(particle_eta);
            output_ntuple->had_mass_.push_back(particle_mass);

            // INCREASE COUNTER
            particle_vsize++;
        }

        output_ntuple->vsize_had_ = particle_vsize;
        // End Loop particles

        event_tree->Fill();
    }
    // End of event loop. Statistics and histograms.

    // Print Stats
    pythia.stat();

    // The code can be obtained by looking at the stat output
    // here we will dump all of them
    int nProcesses = 0;
    float sigma = pythia.info.sigmaGen(0);
    float sigma_error = pythia.info.sigmaErr(0);

    vector<std::string> processes;
    vector<double> process_sigma, process_sigma_error;

    double weight_sum = 0;

    for (const auto &[i, name] : pythia.info.procNameM) {
        nProcesses++;
        processes.push_back(name);
        process_sigma.push_back(pythia.info.sigmaGen(i));
        process_sigma_error.push_back(pythia.info.sigmaErr(i));
    }

    weight_sum = pythia.info.weightSum();

    meta_tree->Branch("vsize_proc", &nProcesses);
    meta_tree->Branch("proc", &processes);
    meta_tree->Branch("proc_sigma", &process_sigma);             // mb
    meta_tree->Branch("proc_sigma_error", &process_sigma_error); // mb
    meta_tree->Branch("sigma", &sigma);                          // mb
    meta_tree->Branch("sigma_error", &sigma_error);              // mb
    meta_tree->Branch("weight_sum", &weight_sum);
    meta_tree->Fill();

    //  Write trees
    meta_tree->Write();
    event_tree->Write();

    // Close file
    file->Close();

    // Clear memory
    delete file;
    delete output_ntuple;
}